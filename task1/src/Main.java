public class Main {
    private static final String ALPHABET = "0123456789ABCDEF";

    public static void main(String[] args) {

        if (args.length == 2 && args[0].matches("\\d+") &&
                args[1].matches("^0.+") && ALPHABET.contains(args[1])) {

            System.out.println(itoBase(Integer.parseInt(args[0]), args[1]));
        } else if (args.length == 3 && args[0].matches("(\\d|[ABCDEF])+") && args[1].matches("^0.+") &&
                ALPHABET.contains(args[1]) && args[2].matches("^0.+") && ALPHABET.contains(args[2])
                && checkBase(args[0], args[1])) {

            System.out.println(itoBase(args[0], args[1], args[2]));
        } else {
            System.out.println("usage");
        }

    }

    /**
     * ����� �������� �������������� ����� � ������� ���������
     */
    private static boolean checkBase(String nb, String base) {
        for (int i = 0; i < nb.length(); i++) {
            if (!base.contains(Character.toString(nb.charAt(i)))) {
                return false;
            }
        }
        return true;
    }

    /**
     * ����� ��� �������� �� 10 ������� � ������
     */
    private static String itoBase(int nb, String base) {
        int temp = nb, baseI = base.length(), rem;
        StringBuilder res = new StringBuilder();
        while (temp != 0) {
            rem = temp % baseI;
            res.append(ALPHABET.charAt(rem));
            temp = temp / baseI;
        }

        return res.reverse().toString();
    }

    /**
     * ����� ��� �������� ����� � ����� ������ �������
     */
    private static String itoBase(String nb, String baseSrc, String baseDst) {

        int dst = baseDst.length(), decNumber = 0;

        decNumber = dec(nb, baseSrc);

        int rem;
        StringBuilder res = new StringBuilder();
        while (decNumber != 0) {
            rem = decNumber % dst;
            res.append(ALPHABET.charAt(rem));
            decNumber = decNumber / dst;
        }

        return res.reverse().toString();
    }

    /**
     * ����� ��� �������� ����� �� 1-16 ������� ��������� � 10
     */
    private static int dec(String nb, String baseSrc) {
        int stc = baseSrc.length(), decNumber = 0;
        String number = nb;

        for (int i = 0; i < number.length(); i++) {

            switch (number.charAt(i)) {
                case 'A':
                    decNumber += 10 * Math.pow(stc, number.length() - 1 - i);
                    break;
                case 'B':
                    decNumber += 11 * Math.pow(stc, number.length() - 1 - i);
                    break;
                case 'C':
                    decNumber += 12 * Math.pow(stc, number.length() - 1 - i);
                    break;
                case 'D':
                    decNumber += 13 * Math.pow(stc, number.length() - 1 - i);
                    break;
                case 'E':
                    decNumber += 14 * Math.pow(stc, number.length() - 1 - i);
                    break;
                case 'F':
                    decNumber += 15 * Math.pow(stc, number.length() - 1 - i);
                    break;
                default:
                    decNumber += (number.charAt(i) - '0') * Math.pow(stc, number.length() - 1 - i);
            }
        }
        return decNumber;
    }
}
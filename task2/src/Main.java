import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private static double a, b, c, x1, x2, y1, y2, z1, z2, c1, c2, c3, r, d, gx, gy, gz, hx, hy, hz;
    private static Double t1, t2;

    public static void main(String[] args) {

        String path = args[0];

        String content = parse(path);
        completionCoord(content);
        List<Double> vector = colculateVector(x1, x2, y1, y2, z1, z2);
        completionArgs(vector);
        findAns();
        getResults(vector);
    }

    private static void getResults(List<Double> vec) {
        if (t1 == null) {
            System.out.println("�������� �� �������");
        } else if (t1 != t2){
            gx = x1 + vec.get(0) * t1;
            gy = y1 + vec.get(1) * t1;
            gz = z1 + vec.get(2) * t1;
            hx = x1 + vec.get(0) * t2;
            hy = y1 + vec.get(1) * t2;
            hz = z1 + vec.get(2) * t2;
            System.out.println("[" + gx + ", " + gy + ", " + gz + "], [" + hx  + ", " + hy  + ", " + hz + "]");
        } else {
            gx = x1 + vec.get(0) * t1;
            gy = y1 + vec.get(1) * t1;
            gz = z1 + vec.get(2) * t1;
            System.out.println("[" + gx + ", " + gy + ", " + gz + "]");
        }
    }

    private static String parse(String path) {
        StringBuilder b = null;
        try(FileReader reader = new FileReader(path))
        {
            b = new StringBuilder();
            int c;
            while((c=reader.read()) != -1){
                b.append((char)c);
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        assert b != null;
        return b.toString();
    }

    private static void completionArgs(List<Double> vec) {

        a = Math.pow(vec.get(0), 2) + Math.pow(vec.get(1), 2) + Math.pow(vec.get(2), 2);
        b = 2 * (vec.get(0) * (x1 - c1) + vec.get(1) * (y1 - c2) + vec.get(2) * (z1 - c3));
        c = Math.pow(x1 - c1, 2) + Math.pow(y1 - c2, 2) + Math.pow(z1 - c3, 2) - Math.pow(r, 2);
        d = Math.pow(b, 2) - 4 * a * c;
    }

    private static void findAns() {
        if (d > 0) {
            t1 = (-b + Math.sqrt(d)) / (2 * a);
            t2 = (-b - Math.sqrt(d)) / (2 * a);
        } else if (d < 0) {
            t1 = null;
            t2 = null;
        } else {
            t1 = (-b) / (2 * a);
            t2 = t1;
        }
    }

    private static void completionCoord(String con) {
        String sc = con.substring(con.indexOf("center: [") + 9, con.indexOf(']'));

        String[] scArr = sc.split(",");
        c1 = Double.parseDouble(scArr[0].trim());
        c2 = Double.parseDouble(scArr[1].trim());
        c3 = Double.parseDouble(scArr[2].trim());


        String rad = con.substring(con.indexOf("radius: ") + 8, con.indexOf("}, "));
        r = Double.parseDouble(rad);

        String l1 = con.substring(con.indexOf("{[") + 2, con.indexOf("], ["));
        String[] l1Arr = l1.split(",");
        x1 = Double.parseDouble(l1Arr[0].trim());
        y1 = Double.parseDouble(l1Arr[1].trim());
        z1 = Double.parseDouble(l1Arr[2].trim());

        String l2 = con.substring(con.indexOf("], [") + 4, con.indexOf("]}}"));
        String[] l2Arr = l2.split(",");
        x2 = Double.parseDouble(l2Arr[0].trim());
        y2 = Double.parseDouble(l2Arr[1].trim());
        z2 = Double.parseDouble(l2Arr[2].trim());
    }

    private static List<Double> colculateVector(double x1, double x2, double y1,
                                                double y2, double z1, double z2) {

        List<Double> vec = new ArrayList<Double>();

        vec.add(x2 - x1);
        vec.add(y2 - y1);
        vec.add(z2 - z1);

        return vec;
    }
}



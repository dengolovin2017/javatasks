import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {

        String a = args[0], b = args[1];

        /**
         * ��� ������� ������� �� �������� ��������� "*" � cmd :(
         */

        if (args[0].equals("task4.jar"))
            a = "*";
        if (args[1].equals("task4.jar"))
            b = "*";

        String regexA = getRegex(a), regexB = getRegex(b);

        boolean e1 = Pattern.matches(regexA, b);
        boolean e2 = Pattern.matches(regexB, a);

        if (e1 || e2) {
            System.out.println("��");
        } else {
            System.out.println("��");
        }
    }

    static String getRegex(String str) {
        StringBuilder regex = new StringBuilder();

        if (str == null || str.equals("")) {
            return null;
        }
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '*') {
                regex.append("(.*)");
            } else if (str.charAt(i) == '.' || str.charAt(i) == '+' || str.charAt(i) == '?') {
                regex.append("\\");
                regex.append(str.charAt(i));
            } else {
                regex.append(str.charAt(i));
            }
        }
        return regex.toString();
    }
}